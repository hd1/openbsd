$OpenBSD: patch-hw_scsi_scsi-bus_c,v 1.1 2015/08/11 21:28:53 sthen Exp $

scsi: fix buffer overflow in scsi_req_parse_cdb (CVE-2015-5158)

This is a guest-triggerable buffer overflow present in QEMU 2.2.0
and newer.  scsi_cdb_length returns -1 as an error value, but the
caller does not check it.

Luckily, the massive overflow means that QEMU will just SIGSEGV,
making the impact much smaller.

--- hw/scsi/scsi-bus.c.orig	Wed Aug  5 02:40:58 2015
+++ hw/scsi/scsi-bus.c	Wed Aug  5 02:41:40 2015
@@ -1234,10 +1234,15 @@ int scsi_cdb_length(uint8_t *buf) {
 int scsi_req_parse_cdb(SCSIDevice *dev, SCSICommand *cmd, uint8_t *buf)
 {
     int rc;
+    int len;
 
     cmd->lba = -1;
-    cmd->len = scsi_cdb_length(buf);
+    len = scsi_cdb_length(buf);
+    if (len < 0) {
+        return -1;
+    }
 
+    cmd->len = len;
     switch (dev->type) {
     case TYPE_TAPE:
         rc = scsi_req_stream_xfer(cmd, dev, buf);

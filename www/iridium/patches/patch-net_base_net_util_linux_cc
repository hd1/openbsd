$OpenBSD: patch-net_base_net_util_linux_cc,v 1.2 2015/05/06 14:27:24 robert Exp $
--- net/base/net_util_linux.cc.orig.port	Wed Apr 15 09:36:13 2015
+++ net/base/net_util_linux.cc	Wed Apr 15 15:52:35 2015
@@ -4,12 +4,14 @@
 
 #include "net/base/net_util_linux.h"
 
-#if !defined(OS_ANDROID)
+#if !defined(OS_ANDROID) && !defined(OS_BSD)
 #include <linux/ethtool.h>
 #endif  // !defined(OS_ANDROID)
+#if !defined(OS_BSD)
 #include <linux/if.h>
 #include <linux/sockios.h>
 #include <linux/wireless.h>
+#endif
 #include <set>
 #include <sys/ioctl.h>
 #include <sys/types.h>
@@ -39,6 +41,7 @@ namespace {
 // application layer.
 bool TryConvertNativeToNetIPAttributes(int native_attributes,
                                        int* net_attributes) {
+#if !defined(OS_BSD)
   // For Linux/ChromeOS/Android, we disallow addresses with attributes
   // IFA_F_OPTIMISTIC, IFA_F_DADFAILED, and IFA_F_TENTATIVE as these
   // are still progressing through duplicated address detection (DAD)
@@ -61,6 +64,9 @@ bool TryConvertNativeToNetIPAttributes(int native_attr
   }
 
   return true;
+#else
+  return false;
+#endif
 }
 
 }  // namespace
@@ -79,6 +85,7 @@ inline const unsigned char* GetIPAddressData(const IPA
 // or ethtool extensions.
 NetworkChangeNotifier::ConnectionType GetInterfaceConnectionType(
     const std::string& ifname) {
+#if !defined(OS_BSD)
   base::ScopedFD s(socket(AF_INET, SOCK_STREAM, 0));
   if (!s.is_valid())
     return NetworkChangeNotifier::CONNECTION_UNKNOWN;
@@ -99,11 +106,13 @@ NetworkChangeNotifier::ConnectionType GetInterfaceConn
   if (ioctl(s.get(), SIOCETHTOOL, &ifr) != -1)
     return NetworkChangeNotifier::CONNECTION_ETHERNET;
 #endif  // !defined(OS_ANDROID)
+#endif
 
   return NetworkChangeNotifier::CONNECTION_UNKNOWN;
 }
 
 std::string GetInterfaceSSID(const std::string& ifname) {
+#if !defined(OS_BSD)
   base::ScopedFD ioctl_socket(socket(AF_INET, SOCK_DGRAM, 0));
   if (!ioctl_socket.is_valid())
     return "";
@@ -115,9 +124,11 @@ std::string GetInterfaceSSID(const std::string& ifname
   wreq.u.essid.length = IW_ESSID_MAX_SIZE;
   if (ioctl(ioctl_socket.get(), SIOCGIWESSID, &wreq) != -1)
     return ssid;
+#endif
   return "";
 }
 
+#if !defined(OS_BSD)
 bool GetNetworkListImpl(
     NetworkInterfaceList* networks,
     int policy,
@@ -186,6 +197,7 @@ bool GetNetworkListImpl(
 
   return true;
 }
+#endif
 
 std::string GetWifiSSIDFromInterfaceListInternal(
     const NetworkInterfaceList& interfaces,
@@ -210,12 +222,16 @@ bool GetNetworkList(NetworkInterfaceList* networks, in
   if (networks == NULL)
     return false;
 
+#if !defined(OS_BSD)
   internal::AddressTrackerLinux tracker;
   tracker.Init();
 
   return internal::GetNetworkListImpl(
       networks, policy, tracker.GetOnlineLinks(), tracker.GetAddressMap(),
       &internal::AddressTrackerLinux::GetInterfaceName);
+#else
+  return false;
+#endif
 }
 
 std::string GetWifiSSID() {
